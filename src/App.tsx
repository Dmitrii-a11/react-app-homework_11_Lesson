import './App.css';

export default function App() {

  return (
    <>
      <div className="auth">
        <form className="auth_form">
          <div className="auth_in">
            <h5 className="title offset_mod">Log In</h5>
            <div className="auth_body">
              <div className="auth_w_input">
                <input
                  type="text"
                  className="input"
                  name="email"
                  placeholder="Email"
                  aria-label="email"
                  aria-describedby="addon-wrapping"
                />
              </div>
              <div className="auth_w_input">
                <input
                  type="password"
                  name="password"
                  className="input"
                  placeholder="Password"
                  aria-label="password"
                  aria-describedby="addon-wrapping"
                />
              </div>
            </div>
            <div className="auth_footer">
              <button className="btn primary_state btn_auth">Log In</button>
            </div>
          </div>
        </form>
        <a href="https://ru.wikipedia.org/wiki/%D0%90%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F"
           className="about_link">
          Что такое авторизация?
        </a>
      </div>

    </>
  );
}

// export default App;
